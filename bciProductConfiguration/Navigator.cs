﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Forms;
using System.Net;
using System.ComponentModel;

namespace BciProductConfiguration
{
    // Manage navigation through the various user forms
    public class Navigator : ApplicationContext
    {
        // Host Timeout
        public const int HOST_TIMEOUT_SECONDS = 40;

        // Forms
        private IntroForm _intro_form;
        private IpAddressForm _ip_address_form;
        private SecurityForm _security_form;
        private Form _current_form;

        // Remote hosts
        private BindingList<RemoteHost> _remote_hosts;
        private UdpCommandProcessor _command_processor;
        private CypressListener _cypress_listener;

        // Timer
        private Timer _timer;

        // Properties
        public BindingList<RemoteHost> RemoteHosts
        {
            get
            {
                return _remote_hosts;
            }
        }

        // Constructor
        public Navigator()
        {
            _remote_hosts = new BindingList<RemoteHost>();
            _command_processor = new UdpCommandProcessor();
            _cypress_listener = new CypressListener();

            _intro_form = new IntroForm(this);
            _ip_address_form = new IpAddressForm(this);
            _security_form = new SecurityForm(this);

            _current_form = _intro_form;
            _current_form.Show();

            // Add a processor for discover and ack packets
            _command_processor.NewHelloMessage += command_processor_NewHelloMessage;
            _command_processor.NewAckMessage += command_processor_NewAckMessage;
            _cypress_listener.NewHelloMessage += command_processor_NewHelloMessage;

            // Start a timer for removing inactive hosts
            _timer = new Timer();
            _timer.Interval = 1000;
            _timer.Tick += _timer_Tick;
            _timer.Start();
            
            // Start network activity
            _command_processor.Start();
            try
            {
                _cypress_listener.Start();
            }
            catch
            {
                MessageBox.Show("Problem starting listener thread - perhaps another application has control of the port");
                close_all_forms();
            }
        }

        // Remove inactive host records
        void _timer_Tick(object sender, EventArgs e)
        {
            List<RemoteHost> delete_list = new List<RemoteHost>();

            foreach (RemoteHost rh in _remote_hosts)
            {
                if (DateTime.Now.Subtract(rh.LastSeen).TotalSeconds > HOST_TIMEOUT_SECONDS)
                {
                    delete_list.Add(rh);
                }
            }
            foreach (RemoteHost rh in delete_list)
            {
                _remote_hosts.Remove(rh);
            }
            // If no hosts left - disable the next button
            if (_remote_hosts.Count == 0)
            {
                _intro_form.Block();
            }
        }

        // Process a discover message by updating the remote hosts list
        void command_processor_NewHelloMessage(RemoteHost remote_host)
        {
            // Switch to the GUI thread for any operations that require window updates
            if (_intro_form.InvokeRequired)
            {
                _intro_form.Invoke(new NewHelloMessageDelegate(command_processor_NewHelloMessage), new object[] {remote_host});
                return;
            }

            // Unblock the intro form
            _intro_form.Unblock();

            foreach (RemoteHost rh in _remote_hosts)
            {
                if (rh.MacAddress == remote_host.MacAddress)
                {
                    // Update record
                    rh.RemoteHostIP = remote_host.RemoteHostIP;
                    rh.DhcpEnabled = remote_host.DhcpEnabled;
                    rh.IpAddress = remote_host.IpAddress;
                    rh.SubnetMask = remote_host.SubnetMask;
                    rh.Gateway = remote_host.Gateway;
                    rh.Identifier = remote_host.Identifier;
                    rh.ChassisInfo = remote_host.ChassisInfo;
                    rh.BoardType = remote_host.BoardType;
                    rh.LastSeen = remote_host.LastSeen;
                    sort_units();
                    return;
                }
            }
            // New record
            RemoteHost new_remote_host = new RemoteHost();
            new_remote_host.MacAddress = remote_host.MacAddress;
            new_remote_host.RemoteHostIP = remote_host.RemoteHostIP;
            new_remote_host.DhcpEnabled = remote_host.DhcpEnabled;
            new_remote_host.IpAddress = remote_host.IpAddress;
            new_remote_host.SubnetMask = remote_host.SubnetMask;
            new_remote_host.Gateway = remote_host.Gateway;
            new_remote_host.Identifier = remote_host.Identifier;
            new_remote_host.ChassisInfo = remote_host.ChassisInfo;
            new_remote_host.BoardType = remote_host.BoardType;
            new_remote_host.LastSeen = remote_host.LastSeen;
            _remote_hosts.Add(new_remote_host);
            sort_units();
        }

        // Display a board acknowledgement
        void command_processor_NewAckMessage(IPAddress remote_ip, string mac_address, bool bSuccess)
        {
            // Switch to the GUI thread for any operations that require window updates
            if (_intro_form.InvokeRequired)
            {
                _intro_form.Invoke(new NewAckMessageDelegate(command_processor_NewAckMessage), new object[] { remote_ip, mac_address, bSuccess });
                return;
            }

            if (bSuccess)
            {
                MessageBox.Show("Command accepted by " + remote_ip, "Command Result");
            }
            else
            {
                MessageBox.Show("Command rejected by " + remote_ip, "Command Result");
            }
        }

        // Navigate back to original form
        public void open_intro_form()
        {
            System.Drawing.Point location;

            location = _current_form.Location;
            _current_form.Visible = false;
            _current_form = _intro_form;
            _current_form.Location = location;
            _current_form.Visible = true;
        }

        // Navigate to security form
        public void open_security_form(RemoteHost remote_host)
        {
            System.Drawing.Point location;

            location = _current_form.Location;
            _current_form.Visible = false;
            _current_form = _security_form;
            _current_form.Location = location;
            ((SecurityForm)_current_form).RemoteHost = remote_host;
            _current_form.StartPosition = FormStartPosition.Manual;
            _current_form.Visible = true;
        }

        // Navigate to ip address form
        public void open_ip_address_form(RemoteHost remote_host)
        {
            System.Drawing.Point location;

            location = _current_form.Location;
            _current_form.Visible = false;
            _current_form = _ip_address_form;
            _current_form.Location = location;
            ((IpAddressForm)_current_form).RemoteHost = remote_host;
            _current_form.StartPosition = FormStartPosition.Manual;
            _current_form.Visible = true;
        }

        // Terminate application
        public void close_all_forms()
        {
            ExitThread();
        }

        // Keep the list of discovered units sorted
        public void sort_units()
        {
            // Tried using combo box sorted property but the combo box list quickly became out of sync
            // with the datasource list. Microsoft recommends that the datasource be sorted instead.

            // Check if the list is already good to avoid any GUI updates
            bool bSorted = true;
            for (int i = 0; i < _remote_hosts.Count - 1; i++)
            {
                if (_remote_hosts[i].CompareTo(_remote_hosts[i + 1]) > 0)
                {
                    bSorted = false;
                    break;
                }
            }
            if (bSorted)
            {
                return;
            }
            // Sort the current list
            List<RemoteHost> sort_list = _remote_hosts.ToList();
            sort_list.Sort();
            // Make a note of GUI selected item
            _intro_form.SaveSelection();
            // Change the bound list
            _remote_hosts.RaiseListChangedEvents = false;
            _remote_hosts.Clear();
            foreach (RemoteHost rh in sort_list) {
                _remote_hosts.Add(rh);
            }
            _remote_hosts.RaiseListChangedEvents = true;
            // Notify listeners that the binding list has changed
            _remote_hosts.ResetBindings();
            // Restore the selected item
            _intro_form.RestoreSelection();
        }

        // Send a configuration command for security
        public void issue_config_security_command(RemoteHost remoteHost)
        {
            _command_processor.sendConfigurationPwdPacket(remoteHost.RemoteHostIP,
                remoteHost.MacAddress,
                remoteHost.Password,
                remoteHost.NewPassword,
                remoteHost.Identifier);
        }

        // Send a configuration command for IP address
        public void issue_config_ip_address_command(RemoteHost remoteHost)
        {
            if (remoteHost.BoardType == UdpCommandProcessor.BOARD_TYPE_CYPRESS)
            {
                _cypress_listener.sendConfigurationIpPacket(remoteHost.RemoteHostIP,
                    remoteHost.MacAddress,
                    remoteHost.Password,
                    remoteHost.DhcpEnabled,
                    remoteHost.IpAddress,
                    remoteHost.SubnetMask,
                    remoteHost.Gateway);
                MessageBox.Show("Configuration Updated!", "Command Status");
            }
            else
            {
                _command_processor.sendConfigurationIpPacket(remoteHost.RemoteHostIP,
                    remoteHost.MacAddress,
                    remoteHost.Password,
                    remoteHost.DhcpEnabled,
                    remoteHost.IpAddress,
                    remoteHost.SubnetMask,
                    remoteHost.Gateway);
            }
        }
    }
}
