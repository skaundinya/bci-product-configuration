﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Threading;
using System.Net;

namespace BciProductConfiguration
{
    //
    // Implement Cypress Discovery Protocol Service (CDPS)
    //

    // Manage the discovery of new nodes
    public class CypressListener
    {
        // Events
        public event NewHelloMessageDelegate NewHelloMessage;
        
        // Commands
        public const int COMMAND_POLL = 0x01;
        public const int COMMAND_ACK = 0x02;
        public const int COMMAND_CONFIGURE = 0x03;
        public const int COMMAND_REBOOT = 0x04;

        // Subcommands
        public const int SUBCOMMAND_PRODUCT_ID = 0x01;
        public const int SUBCOMMAND_MAC_ADDRESS = 0x02;
        public const int SUBCOMMAND_IP_ADDRESS = 0x03;
        public const int SUBCOMMAND_SUBNET_MASK = 0x04;
        public const int SUBCOMMAND_GATEWAY = 0x05;
        public const int SUBCOMMAND_DNS_SERVER = 0x06;
        public const int SUBCOMMAND_DHCP_ENABLED = 0x07;
        public const int SUBCOMMAND_WEB_SERVER_PORT = 0x08;
        public const int SUBCOMMAND_TELNET_PORT = 0x09;
        public const int SUBCOMMAND_SERIAL_NUMBER = 0x0A;
        public const int SUBCOMMAND_FIRMWARE_VERSION = 0x0B;
        public const int SUBCOMMAND_HARDWARE_VERSION = 0x0C;
        public const int SUBCOMMAND_PRODUCT_NAME = 0x0D;
        public const int SUBCOMMAND_PRODUCT_DESCRIPTION = 0x0E;

        // Max field lengths
        public const int MAX_PRODUCT_ID_LEN = 2;
        public const int MAX_VERSION_LEN = 10;
        public const int MAX_PRODUCT_NAME_LEN = 20;
        public const int MAX_PRODUCT_DESCRIPTION_LEN = 30;

        // Poll packet
        public const int PP_COMMAND = 0;
        public const int PP_LENGTH = 1;
        public const int PP_CRC = 2;
        public const int PP_PACKET_LEN = 4;

        // Ack packet - payload is full of TLV structures
        public const int AP_COMMAND = 0;
        public const int AP_LENGTH = 1;
        public const int AP_DATA = 2;

        // Configuration packet
        public const int CP_COMMAND = 0;
        public const int CP_LENGTH = 1;
        public const int CP_MAC_TLV = 2;
        public const int CP_IP_TLV = 10;
        public const int CP_SUBNET_MASK_TLV = 16;
        public const int CP_GATEWAY_TLV = 22;
        public const int CP_DHCP_ENABLED_TLV = 28;
        public const int CP_CRC = 31;
        public const int CP_PACKET_LEN = 33;

        // Reboot packet
        public const int RP_COMMAND = 0;
        public const int RP_LENGTH = 1;
        public const int RP_MAC_TLV = 2;
        public const int RP_CRC = 10;
        public const int RP_PACKET_LEN = 12;

        // Multicast address
        public const string CYPRESS_MCAST_ADDRESS = "225.12.13.14";

        // UDP Ports
        public const int CYPRESS_SEND_PORT = 31001;
        public const int CYPRESS_LISTEN_PORT = 31002;

        public const int DISCOVER_INTERVAL_SECONDS = 2;

        // Member variables
        bool _bBroadcastToggle;
        DateTime _last_discover;

         // UDP broadcast Socket
        UdpClient _client_socket;

        // UDP multicast socket
        Socket _mcast_socket;
        IPAddress _mcast_address;
        int _mcast_port;

        // UDP listen socket
        UdpClient _listen_socket;

        // Threads
        Thread _listen_thread;
        Thread _discover_thread;

        // Debugging
        int _debug_count;

        // Constructor
        public CypressListener()
        {
            // Initialize member variables
            _listen_thread = new Thread(new ThreadStart(ListenProc));
            _listen_thread.IsBackground = true;
            _discover_thread = new Thread(new ThreadStart(DiscoverProc));
            _discover_thread.IsBackground = true;
        }

        // Start discovery logic
        public void Start()
        {
            // Create multicast socket
            _mcast_address = IPAddress.Parse(CYPRESS_MCAST_ADDRESS);
            _mcast_port = CYPRESS_SEND_PORT;
            joinMulticastGroup();
            // Create broadcast socket
            _client_socket = new UdpClient(0);
            _client_socket.EnableBroadcast = true;
            // Establishing the listen socket will fail if another application has control of the port
            try
            {
                _listen_socket = new UdpClient(CYPRESS_LISTEN_PORT);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            // Start threads
            _listen_thread.Start();
            _discover_thread.Start();
        }

        // Join a multicast group
        private void joinMulticastGroup()
        {
            try
            {
                // Create a multicast socket.
                _mcast_socket = new Socket(AddressFamily.InterNetwork,
                                         SocketType.Dgram,
                                         ProtocolType.Udp);

                // Get the local IP address used by the listener and the sender to
                // exchange multicast messages. 
                IPAddress localIPAddr = IPAddress.Parse(my_ip_address());

                // Create an IPEndPoint object. 
                IPEndPoint IPlocal = new IPEndPoint(localIPAddr, 0);

                // Bind this endpoint to the multicast socket.
                _mcast_socket.Bind(IPlocal);

                // Define a MulticastOption object specifying the multicast group 
                // address and the local IP address.
                // The multicast group address is the same as the address used by the listener.
                MulticastOption mcastOption;
                mcastOption = new MulticastOption(_mcast_address, localIPAddr);

                _mcast_socket.SetSocketOption(SocketOptionLevel.IP,
                                            SocketOptionName.AddMembership,
                                            mcastOption);

            }
            catch (Exception ex)
            {
                Console.WriteLine("JoinMulticastGroup: Got Exception " + ex.Message);
            }
        }

        #region DISCOVERY_THREAD

        // Poll for new devices on the network
        public void DiscoverProc()
        {
            _last_discover = DateTime.Now.AddSeconds(-1 * DISCOVER_INTERVAL_SECONDS);
            while (1 > 0)
            {
                if (DateTime.Now.Subtract(_last_discover).TotalSeconds >= DISCOVER_INTERVAL_SECONDS)
                {
                    sendDiscoverPacket();
                    _last_discover = DateTime.Now;
                }
                else
                {
                    Thread.Sleep(400);
                }
            }
        }

        // Poll for new devices on the network
        void sendDiscoverPacket()
        {
            _bBroadcastToggle = !_bBroadcastToggle;
            if (_bBroadcastToggle)
            {
                sendBroadcastPollPacket();
            }
            else
            {
                sendMulticastPollPacket();
            }
#if DEBUG
            // Debugging
            _debug_count++;
            if (_debug_count == 2)
            {
                byte[] test_packet = new byte[] {
                    0x02, 0x73, 0x01, 0x02, 0x36, 0x22, 0x02, 0x06, 0xF8, 0x22, 0x85, 0x00, 0x04, 0xB2, 0x03,
                    0x04, 0xAC, 0x10, 0x03, 0x90, 0x04, 0x04, 0xFF, 0xFF, 0xFF, 0x00, 0x05, 0x04, 0xAC, 0x10,
                    0x03, 0x01, 0x06, 0x04, 0x00, 0x00, 0x00, 0x00, 0x07, 0x01, 0x01, 0x08, 0x02, 0x50, 0x00,
                    0x09, 0x02, 0x17, 0x00, 0x0A, 0x08, 0x53, 0x4E, 0x3A, 0x32, 0x32, 0x33, 0x36, 0x00, 0x0B,
                    0x0A, 0x76, 0x32, 0x2E, 0x30, 0x31, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0C, 0x06, 0x76, 0x31,
                    0x2E, 0x30, 0x30, 0x00, 0x0D, 0x0A, 0x43, 0x44, 0x50, 0x57, 0x2D, 0x4B, 0x31, 0x55, 0x53,
                    0x00, 0x0E, 0x18, 0x49, 0x50, 0x20, 0x74, 0x6F, 0x20, 0x42, 0x75, 0x74, 0x74, 0x6F, 0x6E,
                    0x20, 0x57, 0x61, 0x6C, 0x6C, 0x2D, 0x50, 0x6C, 0x61, 0x74, 0x65, 0x00, 0xD6, 0x99
                };
                processClientPacket(test_packet, new IPEndPoint(IPAddress.Parse("169.254.59.200"), 100));
            }
#endif
        }


        // Send broadcast poll packet
        private void sendBroadcastPollPacket()
        {
            Byte[] sendBytes = new Byte[PP_PACKET_LEN];
            sendBytes[PP_COMMAND] = COMMAND_POLL;
            sendBytes[PP_LENGTH] = 0x00;
            sendBytes[PP_CRC] = 0xFF;
            sendBytes[PP_CRC + 1] = 0xFF;
            IPEndPoint remoteEP = new IPEndPoint(IPAddress.Broadcast, CYPRESS_SEND_PORT);
            try
            {
                _client_socket.Send(sendBytes, sendBytes.Length, remoteEP);
            }
            catch (Exception ex)
            {
                Console.WriteLine("SendBroadcastPollPacket: Got Exception " + ex.Message);
            }
        }

        // Send multicast poll packet
        private void sendMulticastPollPacket()
        {
            Byte[] sendBytes = new Byte[PP_PACKET_LEN];
            sendBytes[PP_COMMAND] = COMMAND_POLL;
            sendBytes[PP_LENGTH] = 0x00;
            sendBytes[PP_CRC] = 0xFF;
            sendBytes[PP_CRC + 1] = 0xFF;
            IPEndPoint remoteEP = new IPEndPoint(_mcast_address, _mcast_port);
            try
            {
                _mcast_socket.SendTo(sendBytes, remoteEP);
            }
            catch (Exception ex)
            {
                Console.WriteLine("SendMulticastPollPacket: Got Exception " + ex.Message);
            }
        }

#endregion

#region LISTENING_THREAD

        // Create and manage a UPD socket connection
        public void ListenProc()
        {
            while (1 > 0)
            {
                readClientPacket();
            }
        }

        // Read an process an incoming client packet
        private void readClientPacket()
        {
            // Blocking call to read client packets
            IPEndPoint remoteEP = new IPEndPoint(IPAddress.Any, 0);
            try
            {
                Byte[] receiveBytes = _listen_socket.Receive(ref remoteEP);
                processClientPacket(receiveBytes, remoteEP);
            }
            catch (Exception ex)
            {
                Console.WriteLine("ReadClientPacket: Got Exception " + ex.Message);
            }
        }

        // Print the contents of a client packet
        private void processClientPacket(byte[] packet, IPEndPoint remoteEP)
        {
            if (NewHelloMessage != null)
            {
                // Create new remote host record
                RemoteHost rh = new RemoteHost();
                rh.Identifier = String.Empty;
                // Check minimum packet length
                if (packet.Length < 4)
                {
                    throw new Exception("Short packet length");
                }
                // Check packet type
                if (packet[AP_COMMAND] != COMMAND_ACK)
                {
                    throw new Exception("Unexpected command in received packet");
                }
                // Extract length
                int len = packet[AP_LENGTH];
                if (len != packet.Length - 4)
                {
                    throw new Exception("Unexpected length in received packet");
                }
                // Verify checksum
                ushort checksum_1 = NetworkUtility.calc_crc(0xFFFF, packet, AP_DATA, len);
                ushort checksum_2 = (ushort)(packet[AP_DATA + len] + packet[AP_DATA + len + 1] * 256);
                if (checksum_1 != checksum_2)
                {
                    throw new Exception("Checksum failed in received packet");
                }
                // Process TLV fields
                int index = 0;
                while (index < len)
                {
                    int subcommand = packet[AP_DATA + index];
                    int subcommand_len = packet[AP_DATA + index + 1];
                    int subcommand_data = AP_DATA + index + 2;
                    if (index + subcommand_len + 2 > len)
                    {
                        throw new Exception("Unexpected TLV length in received packet");
                    }
                    if (!process_tlv(rh, packet, subcommand, subcommand_len, subcommand_data))
                    {
                        throw new Exception("Failed to parse TLV field [" + subcommand + "] in received packet");
                    }
                    index += subcommand_len + 2;
                }
                if (index != len)
                {
                    throw new Exception("Unexpected structure in received packet");
                }
                rh.BoardType = UdpCommandProcessor.BOARD_TYPE_CYPRESS;
                rh.ChassisInfo = String.Empty;
                rh.RemoteHostIP = remoteEP.Address;
                rh.LastSeen = DateTime.Now;
                NewHelloMessage(rh);
            }
        }

        // Parse a TLV field in an ACK packet
        private bool process_tlv(RemoteHost rh, byte[] packet, int subcommand, int subcommand_len, int subcommand_data)
        {
            switch (subcommand)
            {
                case SUBCOMMAND_PRODUCT_ID:
                    return parse_product_id(rh, packet, subcommand_data, subcommand_len);
                case SUBCOMMAND_MAC_ADDRESS:
                    return parse_mac_address(rh, packet, subcommand_data, subcommand_len);
                case SUBCOMMAND_IP_ADDRESS:
                    return parse_ip(rh, "IP ADDRESS", packet, subcommand_data, subcommand_len);
                case SUBCOMMAND_SUBNET_MASK:
                    return parse_ip(rh, "SUBNET MASK", packet, subcommand_data, subcommand_len);
                case SUBCOMMAND_GATEWAY:
                    return parse_ip(rh, "GATEWAY", packet, subcommand_data, subcommand_len);
                case SUBCOMMAND_DNS_SERVER:
                    return parse_ip(rh, "DNS SERVER", packet, subcommand_data, subcommand_len);
                case SUBCOMMAND_DHCP_ENABLED:
                    return parse_bool(rh, "DHCP ENABLED", packet, subcommand_data, subcommand_len);
                case SUBCOMMAND_WEB_SERVER_PORT:
                    return parse_port(rh, "WEB SERVER PORT", packet, subcommand_data, subcommand_len);
                case SUBCOMMAND_TELNET_PORT:
                    return parse_port(rh, "TELNET PORT", packet, subcommand_data, subcommand_len);
                case SUBCOMMAND_SERIAL_NUMBER:
                    return parse_string(rh, "SERIAL NUMBER", 10, packet, subcommand_data, subcommand_len);
                case SUBCOMMAND_FIRMWARE_VERSION:
                    return parse_string(rh, "FIRMWARE VERSION", 10, packet, subcommand_data, subcommand_len);
                case SUBCOMMAND_HARDWARE_VERSION:
                    return parse_string(rh, "HARDWARE VERSION", 10, packet, subcommand_data, subcommand_len);
                case SUBCOMMAND_PRODUCT_NAME:
                    return parse_string(rh, "PRODUCT NAME", 20, packet, subcommand_data, subcommand_len);
                case SUBCOMMAND_PRODUCT_DESCRIPTION:
                    return parse_string(rh, "PRODUCT DESCRIPTION", 30, packet, subcommand_data, subcommand_len);
            }
            return false;
        }

        // Parse a 16 bit product ID
        private bool parse_product_id(RemoteHost rh, byte[] packet, int index, int len)
        {
            if (len != 2)
            {
                return false;
            }
            string parse_result = String.Format("> Product ID = {0:X2}:{1:X2}", packet[index + 1], packet[index]);
            Console.WriteLine(parse_result);
            return true;
        }

        // Parse a 6 byte MAC address
        private bool parse_mac_address(RemoteHost rh, byte[] packet, int index, int len)
        {
            if (len != 6)
            {
                return false;
            }
            string parse_result = String.Format("> MAC Address = {0:X2}:{1:X2}:{2:X2}:{3:X2}:{4:X2}:{5:X2}",
                packet[index],
                packet[index + 1],
                packet[index + 2],
                packet[index + 3],
                packet[index + 4],
                packet[index + 5]);
            Console.WriteLine(parse_result);
            rh.MacAddress = NetworkUtility.MacAddressToString(packet, index);
            return true;
        }

        // Parse an IP address field
        private bool parse_ip(RemoteHost rh, string field_name, byte[] packet, int index, int len)
        {
            if (len != 4)
            {
                return false;
            }
            try
            {
                string parse_result = String.Format("> {0} = {1}", field_name, NetworkUtility.IpAddressToString(packet, index));
                Console.WriteLine(parse_result);
                if (field_name.Equals("IP ADDRESS"))
                {
                    rh.IpAddress = NetworkUtility.IpAddressToString(packet, index);
                }
                else if (field_name.Equals("SUBNET MASK"))
                {
                    rh.SubnetMask = NetworkUtility.IpAddressToString(packet, index);
                }
                else if (field_name.Equals("GATEWAY"))
                {
                    rh.Gateway = NetworkUtility.IpAddressToString(packet, index);
                }
            }
            catch
            {
                // Problem parsing IP address
                return false;
            }
            return true;
        }

        // Parse a boolean field
        private bool parse_bool(RemoteHost rh, string field_name, byte[] packet, int index, int len)
        {
            if (len != 1)
            {
                return false;
            }
            string parse_result = String.Format("> {0} = {1}", field_name, Convert.ToBoolean(packet[index]));
            Console.WriteLine(parse_result);
            rh.DhcpEnabled = Convert.ToBoolean(packet[index]);
            return true;
        }

        // Parse a port field
        private bool parse_port(RemoteHost rh, string field_name, byte[] packet, int index, int len)
        {
            if (len != 2)
            {
                return false;
            }
            string parse_result = String.Format("> {0} = {1}", field_name, packet[index] + packet[index + 1] * 256);
            Console.WriteLine(parse_result);
            return true;
        }

        // Parse an ASCII string
        private bool parse_string(RemoteHost rh, string field_name, int max_length, byte[] packet, int index, int len)
        {
            if (len > max_length)
            {
                return false;
            }
            try
            {
                string parse_result = String.Format("> {0} = {1}", field_name, NetworkUtility.BytesToString(packet, index, len));
                Console.WriteLine(parse_result);
                if (field_name.Equals("PRODUCT NAME"))
                {
                    rh.Identifier = NetworkUtility.BytesToString(packet, index, len) + " " + rh.Identifier;
                }
                else if (field_name.Equals("SERIAL NUMBER"))
                {
                    rh.Identifier += rh.Identifier + " " + NetworkUtility.BytesToString(packet, index, len);
                }
            }
            catch
            {
                // Problem parsing ASCII string
                return false;
            }
            return true;
        }

#endregion

#region COMMANDS

        // Request a change of IP address
        public void sendConfigurationIpPacket(IPAddress remote_address, string macAddress, string password, bool dhcpEnabled, string ipAddress, string subnetMask, string gateway)
        {
            // Configure packet
            Byte[] sendBytes_1 = new Byte[CP_PACKET_LEN];
            sendBytes_1[CP_COMMAND] = COMMAND_CONFIGURE;
            sendBytes_1[CP_LENGTH] = (Byte) (sendBytes_1.Length - 4);
            // Mac Address
            sendBytes_1[CP_MAC_TLV] = SUBCOMMAND_MAC_ADDRESS;
            sendBytes_1[CP_MAC_TLV + 1] = 0x06;
            copy_array(sendBytes_1, CP_MAC_TLV + 2, 6, NetworkUtility.MacAddressToBytes(macAddress));
            // IP Address
            sendBytes_1[CP_IP_TLV] = SUBCOMMAND_IP_ADDRESS;
            sendBytes_1[CP_IP_TLV + 1] = 0x04;
            copy_array(sendBytes_1, CP_IP_TLV + 2, 4, NetworkUtility.IpAddressToBytes(ipAddress));
            // Subnet Mask
            sendBytes_1[CP_SUBNET_MASK_TLV] = SUBCOMMAND_SUBNET_MASK;
            sendBytes_1[CP_SUBNET_MASK_TLV + 1] = 0x04;
            copy_array(sendBytes_1, CP_SUBNET_MASK_TLV + 2, 4, NetworkUtility.IpAddressToBytes(subnetMask));
            // Gateway
            sendBytes_1[CP_GATEWAY_TLV] = SUBCOMMAND_GATEWAY;
            sendBytes_1[CP_GATEWAY_TLV + 1] = 0x04;
            copy_array(sendBytes_1, CP_GATEWAY_TLV + 2, 4, NetworkUtility.IpAddressToBytes(gateway));
            // DHCP Enabled
            sendBytes_1[CP_DHCP_ENABLED_TLV] = SUBCOMMAND_DHCP_ENABLED;
            sendBytes_1[CP_DHCP_ENABLED_TLV + 1] = 0x01;
            sendBytes_1[CP_DHCP_ENABLED_TLV + 2] = Convert.ToByte(dhcpEnabled);
            // CRC check
            ushort checksum = NetworkUtility.calc_crc(0xFFFF, sendBytes_1, CP_MAC_TLV, sendBytes_1.Length - 4);
            sendBytes_1[CP_CRC] = (Byte)(checksum & 0xFF);
            sendBytes_1[CP_CRC + 1] = (Byte)((checksum >> 8) & 0xFF);
            // Remote address
            IPEndPoint remoteEP = new IPEndPoint(remote_address, CYPRESS_SEND_PORT);
            try
            {
                UdpClient temp_client = new UdpClient();
                temp_client.Send(sendBytes_1, sendBytes_1.Length, remoteEP);
                Console.WriteLine("*** Sent Configuration Packet");
            }
            catch (Exception ex)
            {
                Console.WriteLine("SendConfigurationPacket: Got Exception " + ex.Message);
            }
            // Short Sleep
            Thread.Sleep(100);
            // Reboot packet
            Byte[] sendBytes_2 = new Byte[RP_PACKET_LEN];
            sendBytes_2[RP_COMMAND] = COMMAND_REBOOT;
            sendBytes_2[RP_LENGTH] = (Byte) (sendBytes_2.Length - 4);
            // Mac Address
            sendBytes_2[RP_MAC_TLV] = SUBCOMMAND_MAC_ADDRESS;
            sendBytes_2[RP_MAC_TLV + 1] = 0x06;
            copy_array(sendBytes_2, RP_MAC_TLV + 2, 6, NetworkUtility.MacAddressToBytes(macAddress));
            // CRC check
            checksum = NetworkUtility.calc_crc(0xFFFF, sendBytes_2, RP_MAC_TLV, sendBytes_2.Length - 4);
            sendBytes_2[RP_CRC] = (Byte)(checksum & 0xFF);
            sendBytes_2[RP_CRC + 1] = (Byte)((checksum >> 8) & 0xFF);
            try
            {
                UdpClient temp_client = new UdpClient();
                temp_client.Send(sendBytes_2, sendBytes_2.Length, remoteEP);
                Console.WriteLine("*** Sent Reboot Packet");
            }
            catch (Exception ex)
            {
                Console.WriteLine("SendConfigurationPacket: Got Exception " + ex.Message);
            }
        }

#endregion

#region UTILITY

        // My IP address
        string my_ip_address()
        {
            // Suggested approach from Stack Overflow web site
            return NetworkInterface.GetAllNetworkInterfaces()
                .SelectMany(adapter => adapter.GetIPProperties().UnicastAddresses)
                .Where(adr => adr.Address.AddressFamily == AddressFamily.InterNetwork && adr.IsDnsEligible)
                .Select(adr => adr.Address.ToString()).FirstOrDefault();
        }

        // Copy from a source array to destination array
        private void copy_array(byte[] destination, int dest_index, int count, byte[] source)
        {
            int source_index = 0;
            Array.Copy(source, source_index, destination, dest_index, count);
        }

#endregion


    }
}
