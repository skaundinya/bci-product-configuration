﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Threading;
using System.Net.Sockets;
using System.Net;

namespace BciProductConfiguration
{
    // Delegates
    public delegate void NewHelloMessageDelegate(RemoteHost rh);
    public delegate void NewAckMessageDelegate(IPAddress remote_ip, string mac_address, bool bSuccess);

    // Manage the discovery of new nodes
    public class UdpCommandProcessor
    {
        // Events
        public event NewHelloMessageDelegate NewHelloMessage;
        public event NewAckMessageDelegate NewAckMessage;

        #region PIC_Packet_Definitions

        // Field sizes
        public const int MAX_BOARD_IDENTIFIER = 30;
        public const int MAX_BOARD_PASSWORD = 30;

        // Discover packet
        public const int DP_PROTOCOL_FLAG = 0;
        public const int DP_PACKET_TYPE = 2;
        public const int DP_PACKET_LEN = 3;

        // Hello packet
        public const int HP_PROTOCOL_FLAG = 0;
        public const int HP_PACKET_TYPE = 2;
        public const int HP_MAC_ADDRESS = 3;
        public const int HP_DHCP_ENABLED = 9;
        public const int HP_IP_ADDRESS = 10;
        public const int HP_SUBNET_MASK = 14;
        public const int HP_GATEWAY = 18;
        public const int HP_BOARD_IDENTIFIER = 22;
        public const int HP_CHASSIS_INFO = 52;
        public const int HP_BOARD_TYPE = 53;
        public const int HP_PACKET_LEN = 54;

        // Configuration packet
        public const int CP_PROTOCOL_FLAG = 0;
        public const int CP_PACKET_TYPE = 2;
        public const int CP_MAC_ADDRESS = 3;
        public const int CP_DHCP_ENABLED = 9;
        public const int CP_IP_ADDRESS = 13;
        public const int CP_SUBNET_MASK = 17;
        public const int CP_GATEWAY = 21;
        public const int CP_BOARD_IDENTIFIER = 25;
        public const int CP_OLD_PASSWORD = 55;
        public const int CP_NEW_PASSWORD = 85;
        public const int CP_PACKET_LEN = 115;

        // ACK packet
        public const int KP_PROTOCOL_FLAG = 0;
        public const int KP_PACKET_TYPE = 2;
        public const int KP_MAC_ADDRESS = 3;
        public const int KP_PACKET_OK = 9;
        public const int KP_PACKET_LEN = 10;

        // Max size of packet
        public const int MAX_PACKET = CP_PACKET_LEN;

        // Packet types
        public const int PACKET_TYPE_DISCOVER = 100;
        public const int PACKET_TYPE_HELLO = 101;
        public const int PACKET_TYPE_CONFIGURATION_IP = 102;
        public const int PACKET_TYPE_CONFIGURATION_PWD = 103;
        public const int PACKET_TYPE_ACK = 104;

        // Protocol flag bytes
        public const int PROTOCOL_FLAG_0 = 0xBB;
        public const int PROTOCOL_FLAG_1 = 0x12;

        // Board type
        public const int BOARD_TYPE_E2S = 0x01;
        public const int BOARD_TYPE_ICP = 0x02;
        public const int BOARD_TYPE_CUANBO = 0x03;
        public const int BOARD_TYPE_CYPRESS = 0x04;

        // UDP Ports
        public const int UDP_PORT_BOARD_LISTEN = 49112;

        #endregion

        public const int DISCOVER_INTERVAL_SECONDS = 10;

        // Socket
        UdpClient _client_socket;
        DateTime _last_discover;

        // Threads
        Thread _listen_thread;
        Thread _discover_thread;

        // Constructor
        public UdpCommandProcessor()
        {
            _listen_thread = new Thread(new ThreadStart(ListenProc));
            _listen_thread.IsBackground = true;
            _discover_thread = new Thread(new ThreadStart(DiscoverProc));
            _discover_thread.IsBackground = true;
        }

        // Start servicing UPD operations
        public void Start()
        {
            // Create socket
            _client_socket = new UdpClient(0);
            _client_socket.EnableBroadcast = true;
            // Create servicing threads
            _listen_thread.Start();
            _discover_thread.Start();
        }

        // Create and manage a UPD socket connection
        public void ListenProc()
        {
            while (1 > 0)
            {
                readClientPacket();
            }
        }

        // Periodically send discover packets
        public void DiscoverProc()
        {
            _last_discover = DateTime.Now.AddSeconds(-1 * DISCOVER_INTERVAL_SECONDS);
            while (1 > 0)
            {
                if (DateTime.Now.Subtract(_last_discover).TotalSeconds >= DISCOVER_INTERVAL_SECONDS)
                {
                    sendDiscoverPacket();
                    _last_discover = DateTime.Now;
                }
                else
                {
                    Thread.Sleep(400);
                }
            }
        }

        // Read an process an incoming client packet
        private void readClientPacket()
        {
            // Blocking call to read client packets
            IPEndPoint remoteEP = new IPEndPoint(IPAddress.Any, UDP_PORT_BOARD_LISTEN);
            try
            {
                Byte[] receiveBytes = _client_socket.Receive(ref remoteEP);
                Console.WriteLine("*** Got Packet");
                if (receiveBytes.Length > 3 && receiveBytes.Length <= MAX_PACKET)
                {
                    if (receiveBytes[DP_PROTOCOL_FLAG] == PROTOCOL_FLAG_0 && receiveBytes[DP_PROTOCOL_FLAG + 1] == PROTOCOL_FLAG_1)
                    {
                        switch (receiveBytes[DP_PACKET_TYPE])
                        {
                            case PACKET_TYPE_HELLO:
                                if (receiveBytes.Length == HP_PACKET_LEN)
                                {
                                    processHelloPacket(remoteEP, receiveBytes);
                                }
                                break;
                            case PACKET_TYPE_ACK:
                                if (receiveBytes.Length == KP_PACKET_LEN)
                                {
                                    processAckPacket(remoteEP, receiveBytes);
                                }
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("ReadClientPacket: Got Exception " + ex.Message);
            }
        }

        // Received a hello packet from a remote host
        private void processHelloPacket(IPEndPoint remoteEP, byte[] receiveBytes)
        {
            Console.WriteLine("*** Got Hello Packet");
            if (NewHelloMessage != null)
            {
                RemoteHost rh = new RemoteHost();
                rh.MacAddress = NetworkUtility.MacAddressToString(receiveBytes, HP_MAC_ADDRESS);
                rh.DhcpEnabled = Convert.ToBoolean(receiveBytes[HP_DHCP_ENABLED]);
                rh.IpAddress = NetworkUtility.IpAddressToString(receiveBytes, HP_IP_ADDRESS);
                rh.SubnetMask = NetworkUtility.IpAddressToString(receiveBytes, HP_SUBNET_MASK);
                rh.Gateway = NetworkUtility.IpAddressToString(receiveBytes, HP_GATEWAY);
                rh.Identifier = NetworkUtility.BytesToString(receiveBytes, HP_BOARD_IDENTIFIER, MAX_BOARD_IDENTIFIER);
                rh.BoardType = receiveBytes[HP_BOARD_TYPE];
                // Chassis info is only relevant for E2S boards
                if (rh.BoardType == BOARD_TYPE_E2S)
                {
                    rh.ChassisInfo = NetworkUtility.ChassisInfoToString(receiveBytes[HP_CHASSIS_INFO]);
                }
                else
                {
                    rh.ChassisInfo = String.Empty;
                }
                rh.RemoteHostIP = remoteEP.Address;
                rh.LastSeen = DateTime.Now;
                NewHelloMessage(rh);
            }
        }

        // Received an ACK packet from a remote host
        private void processAckPacket(IPEndPoint remoteEP, byte[] receiveBytes)
        {
            Console.WriteLine("*** Got ACK Packet");
            if (NewAckMessage != null)
            {
                string mac_address = NetworkUtility.MacAddressToString(receiveBytes, KP_MAC_ADDRESS);
                bool bSuccess = Convert.ToBoolean(receiveBytes[KP_PACKET_OK]);
                NewAckMessage(remoteEP.Address, mac_address, bSuccess);
            }
        }

        // Send a discover packet
        private void sendDiscoverPacket()
        {
            Byte[] sendBytes = new Byte[DP_PACKET_LEN];
            sendBytes[DP_PROTOCOL_FLAG] = PROTOCOL_FLAG_0;
            sendBytes[DP_PROTOCOL_FLAG + 1] = PROTOCOL_FLAG_1;
            sendBytes[DP_PACKET_TYPE] = PACKET_TYPE_DISCOVER;
            IPEndPoint remoteEP = new IPEndPoint(IPAddress.Broadcast, UDP_PORT_BOARD_LISTEN);
            try
            {
                _client_socket.Send(sendBytes, sendBytes.Length, remoteEP);
                Console.WriteLine("*** Sent Discover Packet");
            }
            catch (Exception ex)
            {
                Console.WriteLine("SendDiscoverPacket: Got Exception " + ex.Message);
            }
        }

        // Send a configuration packet
        public void sendConfigurationIpPacket(IPAddress remote_address, string mac_address, string password, bool dhcp_enabled, string ip_address, string subnet_mask, string gateway)
        {
            Byte[] sendBytes = new Byte[CP_PACKET_LEN];
            sendBytes[CP_PROTOCOL_FLAG] = PROTOCOL_FLAG_0;
            sendBytes[CP_PROTOCOL_FLAG + 1] = PROTOCOL_FLAG_1;
            sendBytes[CP_PACKET_TYPE] = PACKET_TYPE_CONFIGURATION_IP;
            copy_array(sendBytes, CP_MAC_ADDRESS, 6, NetworkUtility.MacAddressToBytes(mac_address));
            copy_array(sendBytes, CP_OLD_PASSWORD, password.Length, NetworkUtility.StringToBytes(password));
            pad_array(sendBytes, CP_OLD_PASSWORD, password.Length, MAX_BOARD_PASSWORD);
            sendBytes[CP_DHCP_ENABLED] = Convert.ToByte(dhcp_enabled);
            copy_array(sendBytes, CP_IP_ADDRESS, 4, NetworkUtility.IpAddressToBytes(ip_address));
            copy_array(sendBytes, CP_SUBNET_MASK, 4, NetworkUtility.IpAddressToBytes(subnet_mask));
            copy_array(sendBytes, CP_GATEWAY, 4, NetworkUtility.IpAddressToBytes(gateway));
            IPEndPoint remoteEP = new IPEndPoint(remote_address, UDP_PORT_BOARD_LISTEN);
            try
            {
                _client_socket.Send(sendBytes, sendBytes.Length, remoteEP);
                Console.WriteLine("*** Sent Configuration Packet");
            }
            catch (Exception ex)
            {
                Console.WriteLine("SendConfigurationPacket: Got Exception " + ex.Message);
            }
        }

        // Send a configuration packet
        public void sendConfigurationPwdPacket(IPAddress remote_address, string mac_address, string password, string new_password, string identifier)
        {
            Byte[] sendBytes = new Byte[CP_PACKET_LEN];
            sendBytes[CP_PROTOCOL_FLAG] = PROTOCOL_FLAG_0;
            sendBytes[CP_PROTOCOL_FLAG + 1] = PROTOCOL_FLAG_1;
            sendBytes[CP_PACKET_TYPE] = PACKET_TYPE_CONFIGURATION_PWD;
            copy_array(sendBytes, CP_MAC_ADDRESS, 6, NetworkUtility.MacAddressToBytes(mac_address));
            copy_array(sendBytes, CP_OLD_PASSWORD, password.Length, NetworkUtility.StringToBytes(password));
            pad_array(sendBytes, CP_OLD_PASSWORD, password.Length, MAX_BOARD_PASSWORD);
            copy_array(sendBytes, CP_NEW_PASSWORD, new_password.Length, NetworkUtility.StringToBytes(new_password));
            pad_array(sendBytes, CP_NEW_PASSWORD, new_password.Length, MAX_BOARD_PASSWORD);
            copy_array(sendBytes, CP_BOARD_IDENTIFIER, identifier.Length, NetworkUtility.StringToBytes(identifier));
            pad_array(sendBytes, CP_BOARD_IDENTIFIER, identifier.Length, MAX_BOARD_IDENTIFIER);
            IPEndPoint remoteEP = new IPEndPoint(remote_address, UDP_PORT_BOARD_LISTEN);
            try
            {
                _client_socket.Send(sendBytes, sendBytes.Length, remoteEP);
                Console.WriteLine("*** Sent Configuration Packet");
            }
            catch (Exception ex)
            {
                Console.WriteLine("SendConfigurationPacket: Got Exception " + ex.Message);
            }
        }

        // Copy from a source array to destination array
        private void copy_array(byte[] destination, int dest_index, int count, byte[] source)
        {
            int source_index = 0;
            Array.Copy(source, source_index, destination, dest_index, count);
        }

        // Pad empty string space with zeroes
        private void pad_array(byte[] sendBytes, int dest_index, int string_len, int max_length)
        {
            for (int i = string_len; i < max_length; i++)
            {
                sendBytes[dest_index + i] = 0x00;
            }
        }

    }
}
