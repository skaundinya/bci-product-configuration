﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BciProductConfiguration
{
    public partial class IpAddressForm : Form
    {
        // Member variables
        Navigator _navigator;

        // Public properties
        private RemoteHost _remote_host;
        public RemoteHost RemoteHost
        {
            get
            {
                return _remote_host;
            }
            set
            {
                _remote_host = value;
                if (_remote_host.SupportsPasswords)
                {
                    tbPassword.Visible = true;
                    tbPassword.Enabled = true;
                    lblPassword.Visible = true;
                }
                else
                {
                    tbPassword.Visible = false;
                    tbPassword.Enabled = false;
                    lblPassword.Visible = false;
                }
                cbDHCP.Checked = _remote_host.DhcpEnabled;
                tbIpAddress.Text = _remote_host.IpAddress;
                tbSubnetMask.Text = _remote_host.SubnetMask;
                tbGateway.Text = _remote_host.Gateway;
                tbPassword.Clear();
            }
        }

        public IpAddressForm(Navigator navigator)
        {
            _navigator = navigator;
            InitializeComponent();
        }

        private void btnConfigure_Click(object sender, EventArgs e)
        {
            if (input_is_valid())
            {
                // Store updated parameters
                this.RemoteHost.Password = tbPassword.Text;
                this.RemoteHost.DhcpEnabled = cbDHCP.Checked;
                this.RemoteHost.IpAddress = tbIpAddress.Text;
                this.RemoteHost.SubnetMask = tbSubnetMask.Text;
                this.RemoteHost.Gateway = tbGateway.Text;
                _navigator.issue_config_ip_address_command(this.RemoteHost);
                _navigator.open_intro_form();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            _navigator.open_intro_form();
        }

        private void IpAddressForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            _navigator.close_all_forms();
        }

        #region Validation

        private bool input_is_valid()
        {
            ep1.Clear();
            // Check password
            if (RemoteHost.SupportsPasswords)
            {
                if (tbPassword.Text.Length == 0 || tbPassword.Text.Length >= UdpCommandProcessor.MAX_BOARD_PASSWORD)
                {
                    ep1.SetError(tbPassword, "Password length");
                    return false;
                }
                if (!NetworkUtility.isAscii(tbPassword.Text))
                {
                    ep1.SetError(tbPassword, "Password must be ASCII only");
                    return false;
                }
            }
            // Check IP address
            if (!NetworkUtility.isValidIp(tbIpAddress.Text))
            {
                ep1.SetError(tbIpAddress, "Invalid IP address");
                return false;
            }
            // Check subnet address
            if (!NetworkUtility.isValidIp(tbSubnetMask.Text))
            {
                ep1.SetError(tbSubnetMask, "Invalid subnet address");
                return false;
            }
            // Check gateway address
            if (!NetworkUtility.isValidIp(tbGateway.Text))
            {
                ep1.SetError(tbGateway, "Invalid gateway address");
                return false;
            }
            return true;
        }

        #endregion

        private void cbDHCP_CheckedChanged(object sender, EventArgs e)
        {
            if (cbDHCP.Checked)
            {
                tbIpAddress.Enabled = false;
                tbSubnetMask.Enabled = false;
                tbGateway.Enabled = false;
            }
            else
            {
                tbIpAddress.Enabled = true;
                tbSubnetMask.Enabled = true;
                tbGateway.Enabled = true;
            }
        }

    }
}
