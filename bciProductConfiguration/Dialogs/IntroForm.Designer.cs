﻿namespace BciProductConfiguration
{
    partial class IntroForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IntroForm));
            this.lblTitle = new System.Windows.Forms.Label();
            this.cboUnit = new System.Windows.Forms.ComboBox();
            this.cboTask = new System.Windows.Forms.ComboBox();
            this.lblSelectUnit = new System.Windows.Forms.Label();
            this.lblSelectTask = new System.Windows.Forms.Label();
            this.btnNext = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(15, 17);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(485, 46);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "BCI Product Configuration";
            // 
            // cboUnit
            // 
            this.cboUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboUnit.FormattingEnabled = true;
            this.cboUnit.Items.AddRange(new object[] {
            "192.168.1.200 (New York)"});
            this.cboUnit.Location = new System.Drawing.Point(250, 100);
            this.cboUnit.Name = "cboUnit";
            this.cboUnit.Size = new System.Drawing.Size(350, 24);
            this.cboUnit.TabIndex = 1;
            // 
            // cboTask
            // 
            this.cboTask.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTask.FormattingEnabled = true;
            this.cboTask.Items.AddRange(new object[] {
            "Set IP Address",
            "Set Security"});
            this.cboTask.Location = new System.Drawing.Point(250, 150);
            this.cboTask.Name = "cboTask";
            this.cboTask.Size = new System.Drawing.Size(350, 24);
            this.cboTask.TabIndex = 2;
            // 
            // lblSelectUnit
            // 
            this.lblSelectUnit.AutoSize = true;
            this.lblSelectUnit.Location = new System.Drawing.Point(15, 100);
            this.lblSelectUnit.Name = "lblSelectUnit";
            this.lblSelectUnit.Size = new System.Drawing.Size(169, 17);
            this.lblSelectUnit.TabIndex = 3;
            this.lblSelectUnit.Text = "Select a unit to configure:";
            // 
            // lblSelectTask
            // 
            this.lblSelectTask.AutoSize = true;
            this.lblSelectTask.Location = new System.Drawing.Point(15, 150);
            this.lblSelectTask.Name = "lblSelectTask";
            this.lblSelectTask.Size = new System.Drawing.Size(179, 17);
            this.lblSelectTask.TabIndex = 4;
            this.lblSelectTask.Text = "Select a configuration task:";
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNext.AutoSize = true;
            this.btnNext.Enabled = false;
            this.btnNext.Location = new System.Drawing.Point(695, 416);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 27);
            this.btnNext.TabIndex = 5;
            this.btnNext.Text = "Next";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // IntroForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(782, 455);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.lblSelectTask);
            this.Controls.Add(this.lblSelectUnit);
            this.Controls.Add(this.cboTask);
            this.Controls.Add(this.cboUnit);
            this.Controls.Add(this.lblTitle);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "IntroForm";
            this.Text = "BCI Product Configuration 2.0";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.IntroForm_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.ComboBox cboUnit;
        private System.Windows.Forms.ComboBox cboTask;
        private System.Windows.Forms.Label lblSelectUnit;
        private System.Windows.Forms.Label lblSelectTask;
        private System.Windows.Forms.Button btnNext;
    }
}

