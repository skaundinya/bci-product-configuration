﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BciProductConfiguration
{
    public partial class SecurityForm : Form
    {
        // Member variables
        private Navigator _navigator;
        
        // Public properties
        private RemoteHost _remote_host;
        public RemoteHost RemoteHost
        {
            get
            {
                return _remote_host;
            }
            set
            {
                _remote_host = value;
                if (_remote_host.SupportsPasswords)
                {
                    btnConfigure.Visible = true;
                    btnConfigure.Enabled = true;
                    tbCurrentPassword.Visible = true;
                    tbCurrentPassword.Enabled = true;
                    lblCurrentPassword.Visible = true;
                    tbNewPassword_1.Visible = true;
                    tbNewPassword_1.Enabled = true;
                    lblNewPassword_1.Visible = true;
                    tbNewPassword_2.Visible = true;
                    tbNewPassword_2.Enabled = true;
                    lblNewPassword_2.Visible = true;
                    tbName.Enabled = true;
                }
                else
                {
                    btnConfigure.Visible = false;
                    btnConfigure.Enabled = false;
                    tbCurrentPassword.Visible = false;
                    tbCurrentPassword.Enabled = false;
                    lblCurrentPassword.Visible = false;
                    tbNewPassword_1.Visible = false;
                    tbNewPassword_1.Enabled = false;
                    lblNewPassword_1.Visible = false;
                    tbNewPassword_2.Visible = false;
                    tbNewPassword_2.Enabled = false;
                    lblNewPassword_2.Visible = false;
                    tbName.Enabled = false;
                }
                tbName.Text = value.Identifier;
                tbCurrentPassword.Clear();
                tbNewPassword_1.Clear();
                tbNewPassword_2.Clear();
            }
        }

        public SecurityForm(Navigator navigator)
        {
            _navigator = navigator;
            InitializeComponent();
        }

        private void btnConfigure_Click(object sender, EventArgs e)
        {
            if (input_is_valid())
            {
                // Store updated parameters
                this.RemoteHost.Password = tbCurrentPassword.Text;
                this.RemoteHost.NewPassword = tbNewPassword_1.Text;
                this.RemoteHost.Identifier = tbName.Text;
                _navigator.sort_units();
                _navigator.issue_config_security_command(this.RemoteHost);
                _navigator.open_intro_form();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            _navigator.open_intro_form();
        }

        private void SecurityForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            _navigator.close_all_forms();
        }

        #region Validation

        private bool input_is_valid()
        {
            ep1.Clear();
            // Check old password
            if (tbCurrentPassword.Text.Length == 0 || tbCurrentPassword.Text.Length >= UdpCommandProcessor.MAX_BOARD_PASSWORD)
            {
                ep1.SetError(tbCurrentPassword, "Password length");
                return false;
            }
            if (!NetworkUtility.isAscii(tbCurrentPassword.Text))
            {
                ep1.SetError(tbCurrentPassword, "Password must be ASCII only");
                return false;
            }
            // Check new password
            if (tbNewPassword_1.Text.Length == 0 || tbNewPassword_1.Text.Length >= UdpCommandProcessor.MAX_BOARD_PASSWORD)
            {
                ep1.SetError(tbNewPassword_1, "New password length");
                return false;
            }
            if (!NetworkUtility.isAscii(tbNewPassword_1.Text))
            {
                ep1.SetError(tbNewPassword_1, "New password must be ASCII only");
                return false;
            }
            // Check matching passwords
            if (tbNewPassword_1.Text != tbNewPassword_2.Text)
            {
                ep1.SetError(tbNewPassword_2, "Passwords do not match");
                return false;
            }
            // Check identifier
            if (tbName.Text.Length == 0 || tbName.Text.Length >= UdpCommandProcessor.MAX_BOARD_IDENTIFIER)
            {
                ep1.SetError(tbName, "Unit name length");
                return false;
            }
            if (!NetworkUtility.isAscii(tbCurrentPassword.Text))
            {
                ep1.SetError(tbName, "Unit name must be ASCII only");
                return false;
            }
            return true;
        }

        #endregion

    }
}
