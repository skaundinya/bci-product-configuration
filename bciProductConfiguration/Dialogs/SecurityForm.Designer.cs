﻿namespace BciProductConfiguration
{
    partial class SecurityForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SecurityForm));
            this.lblTitle = new System.Windows.Forms.Label();
            this.lblCurrentPassword = new System.Windows.Forms.Label();
            this.tbCurrentPassword = new System.Windows.Forms.TextBox();
            this.lblNewPassword_1 = new System.Windows.Forms.Label();
            this.tbNewPassword_1 = new System.Windows.Forms.TextBox();
            this.lblNewPassword_2 = new System.Windows.Forms.Label();
            this.tbNewPassword_2 = new System.Windows.Forms.TextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnConfigure = new System.Windows.Forms.Button();
            this.lblName = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.ep1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ep1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(15, 17);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(414, 46);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "Security Configuration";
            // 
            // lblCurrentPassword
            // 
            this.lblCurrentPassword.AutoSize = true;
            this.lblCurrentPassword.Location = new System.Drawing.Point(15, 100);
            this.lblCurrentPassword.Name = "lblCurrentPassword";
            this.lblCurrentPassword.Size = new System.Drawing.Size(124, 17);
            this.lblCurrentPassword.TabIndex = 1;
            this.lblCurrentPassword.Text = "Current Password:";
            // 
            // tbCurrentPassword
            // 
            this.tbCurrentPassword.Location = new System.Drawing.Point(210, 100);
            this.tbCurrentPassword.Name = "tbCurrentPassword";
            this.tbCurrentPassword.Size = new System.Drawing.Size(150, 22);
            this.tbCurrentPassword.TabIndex = 2;
            // 
            // lblNewPassword_1
            // 
            this.lblNewPassword_1.AutoSize = true;
            this.lblNewPassword_1.Location = new System.Drawing.Point(15, 150);
            this.lblNewPassword_1.Name = "lblNewPassword_1";
            this.lblNewPassword_1.Size = new System.Drawing.Size(104, 17);
            this.lblNewPassword_1.TabIndex = 3;
            this.lblNewPassword_1.Text = "New Password:";
            // 
            // tbNewPassword_1
            // 
            this.tbNewPassword_1.Location = new System.Drawing.Point(210, 150);
            this.tbNewPassword_1.Name = "tbNewPassword_1";
            this.tbNewPassword_1.Size = new System.Drawing.Size(150, 22);
            this.tbNewPassword_1.TabIndex = 4;
            // 
            // lblNewPassword_2
            // 
            this.lblNewPassword_2.AutoSize = true;
            this.lblNewPassword_2.Location = new System.Drawing.Point(15, 200);
            this.lblNewPassword_2.Name = "lblNewPassword_2";
            this.lblNewPassword_2.Size = new System.Drawing.Size(156, 17);
            this.lblNewPassword_2.TabIndex = 5;
            this.lblNewPassword_2.Text = "Confirm New Password:";
            // 
            // tbNewPassword_2
            // 
            this.tbNewPassword_2.Location = new System.Drawing.Point(210, 200);
            this.tbNewPassword_2.Name = "tbNewPassword_2";
            this.tbNewPassword_2.Size = new System.Drawing.Size(150, 22);
            this.tbNewPassword_2.TabIndex = 6;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.AutoSize = true;
            this.btnCancel.Location = new System.Drawing.Point(12, 416);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 27);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnConfigure
            // 
            this.btnConfigure.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConfigure.AutoSize = true;
            this.btnConfigure.Location = new System.Drawing.Point(691, 416);
            this.btnConfigure.Name = "btnConfigure";
            this.btnConfigure.Size = new System.Drawing.Size(79, 27);
            this.btnConfigure.TabIndex = 8;
            this.btnConfigure.Text = "Configure";
            this.btnConfigure.UseVisualStyleBackColor = true;
            this.btnConfigure.Click += new System.EventHandler(this.btnConfigure_Click);
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(15, 250);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(78, 17);
            this.lblName.TabIndex = 9;
            this.lblName.Text = "Unit Name:";
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(210, 250);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(250, 22);
            this.tbName.TabIndex = 10;
            // 
            // ep1
            // 
            this.ep1.ContainerControl = this;
            // 
            // SecurityForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(782, 455);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.btnConfigure);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.tbNewPassword_2);
            this.Controls.Add(this.lblNewPassword_2);
            this.Controls.Add(this.tbNewPassword_1);
            this.Controls.Add(this.lblNewPassword_1);
            this.Controls.Add(this.tbCurrentPassword);
            this.Controls.Add(this.lblCurrentPassword);
            this.Controls.Add(this.lblTitle);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SecurityForm";
            this.Text = "Security Configuration";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.SecurityForm_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.ep1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label lblCurrentPassword;
        private System.Windows.Forms.TextBox tbCurrentPassword;
        private System.Windows.Forms.Label lblNewPassword_1;
        private System.Windows.Forms.TextBox tbNewPassword_1;
        private System.Windows.Forms.Label lblNewPassword_2;
        private System.Windows.Forms.TextBox tbNewPassword_2;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnConfigure;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.ErrorProvider ep1;
    }
}