﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Net;

namespace BciProductConfiguration
{
    public partial class IntroForm : Form
    {
        Navigator _navigator;
        Object _saved_selection;

        public IntroForm(Navigator navigator)
        {
            _navigator = navigator;
            InitializeComponent();
            cboUnit.DataSource = _navigator.RemoteHosts;
            cboUnit.DisplayMember = "DisplayName";
            cboUnit.SelectedIndex = -1;
            cboTask.SelectedIndex = 0;
        }

        private void IntroForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            _navigator.close_all_forms();
        }

        public void Unblock()
        {
            btnNext.Enabled = true;
        }

        public void Block()
        {
            btnNext.Enabled = false;
        }

        // Helps with sorting list of units
        public void SaveSelection()
        {
            _saved_selection = cboUnit.SelectedItem;
        }

        // Helps with sorting list of units
        public void RestoreSelection()
        {
            cboUnit.SelectedItem = _saved_selection;
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            String selected_item = cboTask.SelectedItem as string;
            if (selected_item == "Set IP Address")
            {
                _navigator.open_ip_address_form(cboUnit.SelectedItem as RemoteHost);
            }
            else
            {
                _navigator.open_security_form(cboUnit.SelectedItem as RemoteHost);
            }
        }
    }
}
