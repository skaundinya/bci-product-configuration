﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel;

using System.Net;

namespace BciProductConfiguration
{
    public class RemoteHost : INotifyPropertyChanged, IComparable
    {
        // This method is called by the Set accessor of each property. 
        // The CallerMemberName attribute that is applied to the optional propertyName 
        // parameter causes the property name of the caller to be substituted as an argument. 
        private void NotifyPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        // Some fields raise the property changed event notification so that the GUI gets updated
        private string _identifier;
        public string Identifier
        {
            get
            {
                return _identifier;
            }
            set
            {
                if (_identifier != value)
                {
                    _identifier = value;
                    NotifyPropertyChanged("Identifier");
                }
            }
        }

        private string _chassis_info;
        public string ChassisInfo
        {
            get
            {
                return _chassis_info;
            }
            set
            {
                if (_chassis_info != value)
                {
                    _chassis_info = value;
                    NotifyPropertyChanged("ChassisInfo");
                }
            }
        }

        // Mac Address is unique identifier
        public string MacAddress { get; set; }
        // Remote host provided by discover packet
        public IPAddress RemoteHostIP { get; set; }
        // Password related information
        public string Password { get; set; }
        public string NewPassword { get; set; }
        // IP address related information
        public string IpAddress { get; set; }
        public string SubnetMask { get; set; }
        public string Gateway { get; set; }
        public bool DhcpEnabled { get; set; }
        // Board type
        public int BoardType { get; set; }
        // Age out old records
        public DateTime LastSeen { get; set; }

        // Display name
        public string DisplayName
        {
            get
            {
                if (BoardType == UdpCommandProcessor.BOARD_TYPE_E2S)
                {
                    return String.Format("{0} {1}", Identifier, ChassisInfo);
                }
                else
                {
                    return String.Format("{0}", Identifier);
                }
            }
        }

        // Use the display name to sort
        public int CompareTo(object obj)
        {
            return this.DisplayName.CompareTo((obj as RemoteHost).DisplayName);
        }

        // Configurable flag
        public bool SupportsPasswords
        {
            get
            {
                return (BoardType == UdpCommandProcessor.BOARD_TYPE_ICP || BoardType == UdpCommandProcessor.BOARD_TYPE_E2S ||
                    BoardType == UdpCommandProcessor.BOARD_TYPE_CUANBO);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
